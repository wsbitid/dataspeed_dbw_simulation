![](img/cover_picture.png) 

# Documentation for the Dataspeed ADAS Kit Gazebo/ROS Simulator

This is a new version of the existing Gazebo/ROS simulator `dbw_mkz_simulator`, and is intended to completely replace it. Here is a list of the major changes introduced in this new simulator:

- Dropped support for ROS Indigo / Gazebo 2.x, added support for ROS Melodic / Gazebo 9.x
- Added simulated interface to the new Universal Lat/Lon Controller (ULC) ADAS Kit feature
    - See the User's Guide for the ULC in its ROS interface repository: [https://bitbucket.org/DataspeedInc/dataspeed_ulc_ros/downloads](https://bitbucket.org/DataspeedInc/dataspeed_ulc_ros/downloads)
- Added support for the Pacifica platform

To install `dataspeed_dbw_simulator`, first run the following to set up your computer to accept software from the Dataspeed server:

`$ bash <(wget -q -O - https://bitbucket.org/DataspeedInc/ros_binaries/raw/default/scripts/setup.bash)`

Then install the actual simulator package:

`$ sudo apt-get install ros-$ROS_DISTRO-dataspeed-dbw-simulator`

By default, ROS Kinetic is installed with Gazebo 7.0, and ROS Melodic is installed with Gazebo 9.0. It is recommended to install the latest release of 7.x or 9.x like this:

```
sudo sh -c 'echo "deb http://packages.osrfoundation.org/gazebo/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/gazebo-stable.list'
wget http://packages.osrfoundation.org/gazebo.key -O - | sudo apt-key add -
sudo apt update
sudo apt upgrade
```

PDF documentation for the simulator:

- Version 2.0.0 ([simulator_manual_v2_0_0.pdf](https://bitbucket.org/dataspeedinc/dataspeed_dbw_simulation/raw/master/simulator_manual_v2_0_0.pdf)) \[latest\]
